import React from 'react';
import Note from '../Note/Note';
import Editable from '../Editable/Editable';




export default ({
    notes,
    onNoteClick=() => {}, onEdit=() => {}, onDelete=() => {}
  }) => ( 
  
  
    <ul className="notes">{notes.map(({id, editing, task}) =>
      <li key={id}>
       <Note className="note" onClick={onNoteClick.bind(null, id)}>
          <Editable
           className="editable"
             editing={editing}
             value={task}
             onEdit={onEdit.bind(null, id)} />
          <button  className="delete" onClick={onDelete.bind(null, id)}>x</button>
        </Note>
      </li>


    )}
    
    <div className="ui card">
            <div className="content">
            <i className="right floated like icon"></i>
            <i className="right floated star icon"></i>
            <div className="header">Cute Dog</div>
            <div className="description">
                <p></p>
            </div>
            </div>
            <div className="extra content">
            <span className="left floated like">
                <i className="like icon"></i>
                Like
            </span>
            <span className="right floated star">
                <i className="star icon"></i>
                Favorite
            </span>
            </div>
            </div>
    
    </ul>
  
  )